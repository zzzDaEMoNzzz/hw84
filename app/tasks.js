const express = require('express');
const auth = require('../middleware/auth');
const Task = require('../models/Task');

const router = express.Router();

router.get('/', auth, (req, res) => {
  Task.find({user: req.user._id})
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

router.post('/', auth, (req, res) => {
  const taskData = {
    ...req.body,
    user: req.user._id
  };

  const task = new Task(taskData);

  task.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.put('/:id', auth, async (req, res) => {
  const task = await Task.findById(req.params.id);

  if (!task) {
    return res.send({error: 'There are no tasks with the specified ID'});
  }

  if (String(task.user) !== String(req.user._id)) {
    return res.sendStatus(403);
  }

  const taskData = req.body;
  delete taskData.user;

  try {
    await Task.updateOne({_id: req.params.id}, taskData);
    res.send({message: 'Task updated successfully'});
  } catch (error) {
    res.sendStatus(400);
  }
});

router.delete('/:id', auth, async (req, res) => {
  const task = await Task.findById(req.params.id);

  if (!task) {
    return res.send({error: 'There are no tasks with the specified ID'});
  }

  if (String(task.user) !== String(req.user._id)) {
    return res.sendStatus(403);
  }

  try {
    await Task.deleteOne({_id: req.params.id});
    res.send({message: 'Task deleted successfully'});
  } catch (error) {
    res.sendStatus(400);
  }
});

module.exports = router;