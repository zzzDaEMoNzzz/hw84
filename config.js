module.exports = {
  dbUrl: 'mongodb://localhost/hw84',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
