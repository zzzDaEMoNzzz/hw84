const mongoose = require('mongoose');
const config = require('./config');

const Task = require('./models/Task');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [john, user] = await User.create(
    {username: 'John', password: 'password123', token: '12345678'},
    {username: 'User', password: '123123', token: '87654321'},
  );

  await Task.create(
    {user: john._id, title: 'Task1', description: 'some description', status: 'new'},
    {user: john._id, title: 'Task2', description: 'description', status: 'in_progress'},
    {user: user._id, title: 'Do something', description: 'test', status: 'complete'},
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});